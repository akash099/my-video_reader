import cv2
import numpy as np
import glob

#Create a VideoCapture object and
#read from input file
def video_reader(path):
  cap=cv2.VideoCapture(path)

#define a function for frame counts
  def frame_count():
 
    frameCount = int( cap.get( cv2.CAP_PROP_FRAME_COUNT ))
    print("total frames {}".format( frameCount ))

  frame_count()

# original resolution
  def res():
    frameWidth = int( cap.get( cv2.CAP_PROP_FRAME_WIDTH ))
    frameHeight = int(cap.get( cv2.CAP_PROP_FRAME_HEIGHT ))

    print("Original_res : {}*{}".format(frameWidth,frameHeight))

  res()

#resize frame
  def resize_frame( Width, Height):
    resize = cv2.resize(frame,( Width, Height), interpolation = cv2.INTER_CUBIC)
    return resize

  count = 0
  if(cap.isOpened() == False):
    print("Error opening in video file")

  while (cap.isOpened()):
    ret, frame = cap.read()
 
    if ret == True:
        frame = resize_frame(Width, Height)
        cv2.imshow('Frame', frame)
      
        cv2.imwrite("frame%d.jpg" % count, frame)
        count +=1
     
        if cv2.waitKey(2) & 0xFF == ord('q'):
          break

    else:
      break

  cap.release()

  cv2.destroyAllWindows()

  files = glob.glob("*.jpg")
  files.sort()

   
  x_data = []
  for myFile in files:
      image = cv2.imread(myFile)
      x_data.append(image)

  def start(index,x_data):
      return x_data[index: index+16]
  
  def readFromImage(x_data):
      for i in range(0,len(x_data),16):
          my_array=np.array(start(i,x_data))
          return my_array
  readFromImage(x_data)
  
video_reader(path)